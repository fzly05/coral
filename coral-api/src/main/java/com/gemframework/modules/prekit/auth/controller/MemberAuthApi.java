/********************************************************************************
 * Copyright (c) 2013-2015 Sean.P
 *
 * @author Sean.P   
 * @date 2015年7月8日 上午1:23:56 
 ********************************************************************************/
package com.gemframework.modules.prekit.auth.controller;
import com.gemframework.common.annotation.ApiLog;
import com.gemframework.common.utils.GemValidateUtils;
import com.gemframework.model.common.BaseResultData;
import com.gemframework.modules.prekit.auth.entity.request.AppidAndSecretAuthRequest;
import com.gemframework.modules.prekit.auth.entity.response.AccessTokenResponse;
import com.gemframework.service.MemberService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class MemberAuthApi {

	@Resource
	private MemberService memberService;


	@ApiLog(name = "获取token")
	@GetMapping("getAccessToken")
	public BaseResultData getAccessTokenByAccount(@RequestBody AccessTokenResponse request) {
		AccessTokenResponse response = new AccessTokenResponse();
		//参数校验器
		GemValidateUtils.GemValidate(request);
		return null;
	}

}
